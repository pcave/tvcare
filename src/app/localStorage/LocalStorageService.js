function LocalStorageService() {
	function getData(dataId) {
		return localStorage.getItem(dataId);
	}

	function setData(dataId, value) {
		localStorage.setItem(dataId, value);
	}

	function clearData() {
		localStorage.clear();
	}

	function removeData(dataId) {
		var dataString = getData(dataId);
		if (dataString) {
			localStorage.removeItem(dataId);
		}
		dataString = null;
	}

	return {
		getData: getData,
		setData: setData,
		removeData: removeData,
		clearData: clearData
	};
}
