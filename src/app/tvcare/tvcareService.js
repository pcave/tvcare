function TvcareService() {

	var taskManager = new TVCareTaskManager();
	var onSuccessCallback;
	var onErrorCallback;
	var requestObject = {};

	/**
	 * An attempt to write the service as a promise
	 * @param {type} data
	 * @returns {TvcareService.handleRequest.requestObject}
	 */
	function handleRequest(data) {
		if (data) {
			requestObject["success"] = setSuccessCallback;
			requestObject["error"] = setErrorCallback;
		}

		setTimeout(function() {
			sendRequest(data);
		}, 150);

		return requestObject;
	}

	/**
	 * Set the function to be called if the JCAPS request is successful
	 * @param {type} callback
	 * @returns {TvcareService.requestObject}
	 */
	function setSuccessCallback(callback) {
		onSuccessCallback = callback;
		return requestObject;

	}

	/**
	 * Set the function to be called if the JCAPS request fails
	 * @param {type} callback
	 * @returns {TvcareService.requestObject}
	 */
	function setErrorCallback(callback) {
		onErrorCallback = callback;
		return requestObject;
	}

	function sendRequest(requestID) {
		if (isOptionAvailable(requestID)) {

			taskManager.addTask(requestID);

			if (acct) {
				var taskData = {
					stbmac: getMacAddress(),
					action: "process_hit",
					type: requestID,
					acct: acct,
					scid: getSCID()
				};

				JCapsService.getInstance().createRequest(
					taskData,
					//onSuccess
					handleResult,

					//onError
					handleResult
				);
			}
		}
	}

	function handleResult(data) {
		if (data.errorCode) {
			if (onErrorCallback)
				onErrorCallback(data);
		} else {
			if (onSuccessCallback)
				onSuccessCallback(data);
		}
	}

	function isOptionAvailable(taskId) {
		var isAvailable = taskManager.isOptionAvailable(taskId);
		console.log("isOptionAvailable?", isAvailable);
		return isAvailable;
	}

	function getTimeSinceLastAttempt(taskId) {
		return taskManager.getTimeSinceLastAttempt(taskId);
	}

	//	this.init = init;
	this.handleRequest = handleRequest;
	this.isOptionAvailable = isOptionAvailable;
	this.getTimeSinceLastAttempt = getTimeSinceLastAttempt;
}
