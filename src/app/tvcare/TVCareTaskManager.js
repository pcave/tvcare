function TVCareTaskManager() {
	var storageService = new LocalStorageService();
	var appNamePrefix = DAX_NAME + "_";
	//	storageService.clearData(); //to re-test

	function addTask(taskId) {
		var date = new Date();
		var timeValue = getTimeValue(date);

		if (!isNaN(timeValue))
			storageService.setData(appNamePrefix + taskId, String(timeValue));
	}

	function isOptionAvailable(taskId) {
		var elapsedTime = getTimeSinceLastAttempt(taskId);
		if (elapsedTime === -1) return true;

		//is elapsedTime less than the number of minutes in 24hours (1440)
		var minutesInDay = 1440;
		return elapsedTime >= minutesInDay;
	}

	function getTimeSinceLastAttempt(taskId) {
		var taskData = getTaskData(taskId);
		if (taskData === null)
			return -1;

		var timeValue = Number(taskData);
		var currentTimeValue = getTimeValue(new Date());
		var timeDifference = (currentTimeValue - timeValue);

		return timeDifference;
	}

	function getTimeValue(date) {
		date.setMilliseconds(0);
		var time = Math.floor(date.getTime() / 60000);

		return time;
	}

	function removeTask(taskId) {
		if (taskId)
			storageService.removeData(appNamePrefix + taskId);
	}

	function getTaskData(taskId) {
		return storageService.getData(appNamePrefix + taskId);
	}

	this.addTask = addTask;
	this.removeTask = removeTask;
	this.getTaskData = getTaskData;
	this.isOptionAvailable = isOptionAvailable;
	this.getTimeSinceLastAttempt = getTimeSinceLastAttempt;
}
