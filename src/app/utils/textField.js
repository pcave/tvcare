/* 
 * A simple textfield
 * It will NOT: receive focus, scroll its text, have faded areas or handle keys.
 * Designed to be used for Help's IntroText and Title fields.
 */
function TextField() {

	var container;
	var textField;

	/**
	 * @param {String} id - the ID attribute of the html element to attach the textfield to
	 * @param {String} content - the html content to be shown
	 */
	function init(id, content) {

		container = document.getElementById(id);

		textField = document.createElement("div");
		textField.className = "textFieldContent";
		container.appendChild(textField);

		textField.innerHTML = content;

		return this;
	}

	function setContent(newContent) {
		textField.innerHTML = newContent;
		return this;
	}

	function show() {
		container.style.display = '';
		return this;
	}

	function hide() {
		container.style.display = 'none';
		return this;
	}

	function destroy() {
		container.removeChild(textField);
		container = textField = null;
	}

	//add public data to instance
	this.init = init;
	this.setContent = setContent;
	this.show = show;
	this.hide = hide;
	this.destroy = destroy;

}
