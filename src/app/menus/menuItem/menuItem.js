// (function() {
//	"use strict";

function MenuItem(name, data, nonFocusableRowsAtTop, focusableRows, nonFocusableRowsAtBottom, onNavCallback, onMenuItemFocus, onMenuItemSelect) {
	log.debug("---- " + name + " ----");

	var menulist = new vmDtvLib.List();
	var menuData;
	var menuDataLength;
	var htmlElem;
	var hasFocus;

	function setMenuData(newData) {
		log.debug("---- " + name + " :: setMenuData ....... newData.length: " + newData.length);
		menuData = newData;
		menuDataLength = menuData.length;
	}
	setMenuData(data);

	/**
	 * function onRequestData(start, length, cb)
	 * @callback onRequestData
	 * @param {Number} start	- an integer value. It is the position of an item in the full list. It is from the start of the list and the first item is 0.
	 * @param {Number} length	- The number of items requested. This number or less items will be returned
	 * @param {onDataLoaded} cb - function that will be called with the requested data and will have signature as above
	 */
	function onRequestData(start, length, cb) {
		log.debug("---- " + name + " :: onRequestData ....... start, length: " + start, length);
		var data = [];
		var first = false;
		var last = false;
		var pos;
		for (var i = 0; i < length; i++) {
			pos = start + i;
			if (pos < menuDataLength) {
				data.push(menuData[pos].title);
			}
			if (pos === 0) first = true;
			if (pos == menuDataLength - 1) last = true;
		}
		cb(data, start, menuDataLength, first, last);
	}

	var listConfig = {
		listId: name,
		// set up pages -- don't need to specify any more due to default values
		// for all pages (first/middle/last) we want the same set up
		midPageNumFocusable: focusableRows,
		midPageNumStartUnfocusable: nonFocusableRowsAtTop,
		midPageNumEndUnfocusable: nonFocusableRowsAtBottom,

		rightIsSelect: true,
		onNavLimit: onNavCallback, // pass onNav straight through, so menus will receive list's onNav
		onSelect: onSelect,
		onItemFocus: onItemFocus,
		onRequestData: onRequestData //, onDisplayCell: onDisplayCell
	};

	menulist.init(listConfig); //, onRequestData); //, onDisplayCell);

	htmlElem = document.getElementById(name);

	//        'onSelect', [e, getActiveCellElement(), getActiveCellData()]
	function onSelect(event, activeCellElement, activeCellData) {
		if (onMenuItemSelect)
			onMenuItemSelect(event, activeCellElement, activeCellData);
	}

	function allowGhosting() {
		log.debug("---- allowGhosting ........... name: " + name);
		htmlElem.classList.remove('noGhost');
	}

	function removeGhosting() {
		log.debug("---- removeGhosting ........... name: " + name);
		htmlElem.classList.add('noGhost');
	}

	/**
	 * function onItemFocus(cell, data, isGhost)
	 * @callback onItemFocus
	 * @param {HTMLElement} cell - the HTML element in the DOM that is the cell that contains the item which received focus.  The cell element has the following attributes:
	 *									itemId - This the position in the full list of the item with focus. This is a non negative number. The first line in the list is 0.
	 *									cellId - Non negative number. Position of the cell in the displayed grid which contains the item with focus. The first cell on
	 *									the screen (may  not be focusable) is position 0
	 * @param {object} data - The data for the item. This is as passed to the list via the onRequestData function.
	 * @param {Boolean} isGhost - true if the cell has the ghost highlight
	 */
	function onItemFocus(cell, data, isGhost) {
		if (onMenuItemFocus)
			onMenuItemFocus(cell, data, isGhost);
	}

	function onKey(e) {
		log.debug("==== " + name + " onKey :: key " + e.keyCode);
		menulist.onKey(e); // pass on key handling to this menu's list
	}

	function focusAndShow() {
		hasFocus = true;
		menulist.focusAndShow();
	}

	function show() {
		menulist.show();
	}

	function hideAndBlur() {
		menulist.hideAndBlur();
	}

	function blur() {
		hasFocus = false;
		menulist.blur();
	}

	function focusItem(itemId, reload) {
		menulist.focusItem(itemId, reload);
	}

	function getActiveItemPos() {
		return menulist.activeItemPos;
	}

	function getHasFocus() {
		return hasFocus;
	}

	//add public data to instance
	this.setMenuData = setMenuData;
	this.allowGhosting = allowGhosting;
	this.removeGhosting = removeGhosting;
	this.onKey = onKey;
	this.focusAndShow = focusAndShow;
	this.show = show;
	this.hideAndBlur = hideAndBlur;
	this.blur = blur;
	this.focusItem = focusItem;
	this.getActiveItemPos = getActiveItemPos;
	this.getHasFocus = getHasFocus;

	return this;
	// return menulist;

}

// })();
