//(function() {
//	"use strict";

function Menus() {

	var menu;
	var keyCodes;
	var menuItems;
	var introText;
	var onNavLimitCB;
	var onSelectCB;
	var menusHtml;
	var submenuHtml;
	var menuItem;
	//        var submenu;

	// PUBLIC - 
	function init(data, onNavLimitCallback, onSelectCallback) {
		//		log.debug("---- Menus :: init ....... data : " + data);
		console.log("---- Menus :: init ....... data :", data);
		onNavLimitCB = onNavLimitCallback;
		onSelectCB = onSelectCallback;

		menuItems = data.items;

		menu = new MenuItem('menu1', menuItems, 0, 7, 0, menuOnNavLimit, onMenuItemFocus, makeMenuSelection);
		//		submenu = new MenuItem('submenu1', [], 0, 7, 0, menuOnNavLimit, onMenuItemFocus);

		introText = new TextField();
		introText.init("introText", "");

		menusHtml = document.getElementById("menus");
		submenuHtml = document.getElementById("submenu");


	}

	/**
	 * Divert key press to focused component
	 * @param {event} e the keyEvent containing keycodes
	 */
	function onKey(e) {
		e.stopPropagation();

		var key = e.keyCode;
		log.debug("==== onKey : key " + key);
		keyCodes = vmDtvLib.keyCodes;
		if (menu.getHasFocus()) {
			switch (key) {

				case keyCodes.RIGHT:
				case keyCodes.OK:
					if (menuItem && isOptionAvailable(menuItem.link)) {
						menu.onKey(e);
					}
					break;
				case keyCodes.LEFT:
				case keyCodes.UP:
				case keyCodes.DOWN:
				case keyCodes.CHANNELUP:
				case keyCodes.CHANNELDOWN:
				case keyCodes.SKIPFORWARD:
					menu.onKey(e); // pass key handling on to each menu's list via menuItem handler
					break;
				default:
					vmDtvLib.sound('BONK');
					break;
			}
		} else {
			switch (key) {
				case keyCodes.RIGHT:
				case keyCodes.OK:
					selectSubMenuItem();
					break;
				case keyCodes.LEFT:
					blurSubmenu();
					focusAndShow(); // pass key handling on to each menu's list via menuItem handler
					break;
				default:
					vmDtvLib.sound('BONK');
					break;
			}
		}
	}

	/**
	 * function onNavLimit(keyEvent, cell, data)
	 * @callback onNavLimit
	 * @param {Event} keyEvent		- the key event for the type of navigation (up,down,left, right) and contains the keyEvent for the key pressed.
	 * @param {HTMLElement} cell	- the HTML element in the DOM that is the cell that contains the item with focus. The cell element has the following attributes:
	 *									itemId - This the position in the full list of the item with focus. This is a non negative number. The first line in the list is 0.
	 *									cellId - Non negative number. Position of the cell in the displayed grid which contains the item with focus. The first cell on
	 *									the screen (may  not be focusable) is position 0
	 * @param {object} data			- The data for the item. This is as passed to the list via the onRequestData function.
	 */
	function menuOnNavLimit(keyEvent, cell, data) {
		var key = keyEvent.keyCode;
		keyCodes = vmDtvLib.keyCodes;
		var noise = 'UP_DOWN';

		switch (key) {
			case keyCodes.UP:

				if (onNavLimitCB(keyEvent)) {
					menu.removeGhosting();
					menu.blur();
				}
				break;
			case keyCodes.DOWN:
				// onNavLimitCB(keyEvent); // Bonk
				noise = 'BONK';
				break;
			case keyCodes.LEFT:
				onNavLimitCB(keyEvent); // go back in history stack or exit app
				break;
			case keyCodes.RIGHT:
				// case keyCodes.OK:
				menuItem = getMenuItemFromCell(cell);
				if (isOptionAvailable(menuItem.link)) {
					makeMenuSelection();
				}

				noise = null;
				break;
		}
		if (noise) vmDtvLib.sound(noise);
	}

	function getMenuItemFromCell(cell) {
		var index = cell.getAttribute("itemid");
		return menuItems[index];
	}

	/**
	 * A call back function when the menu focus changes
	 * available params are cell, data, isGhost
	 * @param {html tag} cell the html element
	 */
	function onMenuItemFocus(cell) {
		menuItem = getMenuItemFromCell(cell);
		checkMenuItemOptions(menuItem);
	}

	function checkMenuItemOptions() {
		if (isOptionAvailable(menuItem.link)) {
			introText.setContent(menuItem.intro).show();

			updateSubmenu(menuItem.menu.items[0]);
		} else {
			var responseMsg = getResponseMessage(menuItem.link);
			responseMsg = responseMsg.split("<intro>").join(menuItem.intro);
			introText.setContent(responseMsg).show();
			hideSubmenu();
		}
	}

	function updateSubmenu(config) {
		if (config) {
			var submenuText = config.title;
			submenuHtml.innerHTML = submenuText;
			showSubmenu();
			blurSubmenu();

		} else {
			hideSubmenu();
		}

	}

	function showSubmenu() {
		if (submenuHtml.classList.contains("vdl_hide"))
			submenuHtml.classList.remove("vdl_hide");
	}

	function hideSubmenu() {
		if (!(submenuHtml.classList.contains("vdl_hide")))
			submenuHtml.classList.add("vdl_hide");
	}

	function focusSubmenu() {
		if (submenuHtml.classList.contains("vdl_ListCellGhost"))
			submenuHtml.classList.remove("vdl_ListCellGhost");

		if (!(submenuHtml.classList.contains("vdl_ListCellFocus")))
			submenuHtml.classList.add("vdl_ListCellFocus");

		var submenuBg = document.getElementById("submenuBackground");
		if (!(submenuBg.classList.contains("focused")))
			submenuBg.classList.add("focused");
	}

	function blurSubmenu() {
		if (submenuHtml.classList.contains("vdl_ListCellFocus"))
			submenuHtml.classList.remove("vdl_ListCellFocus");

		if (!(submenuHtml.classList.contains("vdl_ListCellGhost")))
			submenuHtml.classList.add("vdl_ListCellGhost");

		var submenuBg = document.getElementById("submenuBackground");
		if (submenuBg.classList.contains("focused"))
			submenuBg.classList.remove("focused");

	}

	function selectSubMenuItem() {
		var link = menuItem.link;
		onSelectCB(link);

		submenuHtml.blur();
		blur();
	}

	function makeMenuSelection(event, activeCellElement, activeCellData) {
		menu.blur();
		focusSubmenu();
		//introText.setContent(fetchIntroTextForItem()).show();

		vmDtvLib.sound('SELECT');
	}

	// PUBLIC - re-display previous menu 'view'
	function show() {
		menusHtml.classList.remove("vdl_remove"); // vdl_hide
	}

	// PUBLIC - hide all menu components
	function hideAndBlur() {
		menusHtml.classList.add("vdl_remove"); // vdl_hide
		blur();
	}

	// PUBLIC - blur last focused component
	function blur() {
		if (isPlugin)
			window.onKey = null;
		else
			stopHandlingKeys();

		menu.blur();
	}

	// PUBLIC - set focus on last known menu element
	function focusAndShow() {
		//make sure html has focus
		menusHtml.focus();

		menu.focusAndShow();
		show();

		if (isPlugin)
			window.onKey = onKey;
		else
			startHandlingKeys();
	}

	/*
	 * Sets focus to the top of the first menu.
	 * Assumes init has been called & data has been set.
	 * Also shows the menus and starts key handling.
	 */
	function setInitialFocus() {
		setState(null);
	}

	/*
	 * PUBLIC - Returns the current state of the menus, so that this value can be simply
	 * passed back in to setState() to restore this view.
	 * 
	 * @return {String} - comma delimited string of params: focusCol,row1index,row2index,row3index
	 */
	function getState() {
		return null; //String(focusCol + "," + rowFocused[1] + "," + rowFocused[2] + "," + rowFocused[3]);
	}

	/*
	 * PUBLIC - Populate the relevant menus (intro text), and set focus accordingly.
	 * Assumes init has been called & data has been set.
	 * If called with no param, defaults to focus menu1 and top menu entries.
	 * Also shows the menus and starts key handling.
	 * 
	 * @param {String} menuParams - comma delimited string of params: focusCol,row1index,row2index,row3index
	 */
	function setState(menuParams) {
		log.debug("==== setState : menuParams: " + menuParams);

		introText.hide();
		menu.focusItem(0, false);
	}

	function startHandlingKeys() {
		document.addEventListener('keydown', onKey, false);
	}

	function stopHandlingKeys() {
		document.removeEventListener('keydown', onKey, false);
	}

	// expose public methods
	this.init = init;
	this.show = show;
	this.hideAndBlur = hideAndBlur;
	this.blur = blur;
	this.onKey = onKey;
	this.focusAndShow = focusAndShow;
	this.setInitialFocus = setInitialFocus;
	this.getState = getState;
	this.setState = setState;
}


// })();
