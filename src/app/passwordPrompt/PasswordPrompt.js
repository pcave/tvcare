function PasswordPrompt(closePopupCB) {
	var config = {
		fieldsLength: 3,
		timeout: 0.25,
		id: "passwordEntryFields",
		onPasswordValidated: null
	};

	var container;
	var containerHtmlTemplate = vm_h2jTemplates['app_passwordPrompt_passwordPrompt'];
	var cssPasswordPrompt = 'passwordPrompt';
	var multitapfields = [];
	var cssInputFocus = "focus";
	var pwidx = [];
	var focusIndex = 0;
	var popup;
	var passwordErrorPopup;
	var submitButton;
	var states = ["multitap", "doSubmit", "validatePassword", "errorPopup"];
	var state = states[0];
	var allowBackspace = true;

	function init(settings, _pwidx) {
		for (var key in settings) {
			if (config.hasOwnProperty(key)) {
				config[key] = settings[key];
			}
		}

		if (_pwidx.length) {
			pwidx = _pwidx;
			build();
		} else {
			//throw or handle error
			console.log("PasswordPrompt:: init:: no password indexes received");
		}
	}

	function getNumberAsPosition(num) {
		if (num === 1) return String(num + "st");
		if (num === 2) return String(num + "nd");
		if (num === 3) return String(num + "rd");
		if (num >= 4) return String(num + "th");
	}

	/**
	 * @private
	 * attachTemplate Attach the defined HTML template to the container and render in the DOM ]
	 * Uses a library method to create a HTML Element from an inline template
	 * @throws {Error} If The template is missing or has invalid HTML
	 * @throws {Error} If template does not include teh required class
	 */
	function attachTemplate() {
		var fragment = vmDtvLib.createHtmlFrag(containerHtmlTemplate);
		if (!fragment) throw new VmError('PasswordPrompt: attachTemplate : Unable to create valid HTML from provided template ');

		container.appendChild(fragment);
	}

	/**
	 * 
	 * @returns {undefined}
	 */
	function remove() {
		multitapfields = [];

		if (container.firstChild)
			container.removeChild(container.firstChild);

		popup.deletePopup();
		popup = null;
	}

	function build() {

		createPopup();
		createContainer();
		attachTemplate();

		var parentTag = document.getElementsByClassName(config.id)[0];

		if (parentTag) {
			var i = -1;
			while (++i < config.fieldsLength) {
				var fieldContainerDiv = document.createElement("div");
				fieldContainerDiv.className = "PasswordFieldContainer";

				var inputElement = document.createElement("div");
				inputElement.id = 'PasswordField' + i;
				inputElement.className = "PasswordEntryField";

				fieldContainerDiv.appendChild(inputElement);
				parentTag.appendChild(fieldContainerDiv);

				var multitap = new Multitap();
				multitap.setCase("UPPER");
				multitap.init({
					id: 'PasswordField' + i,
					mode: "password",
					passwordVisible: false,
					timeout: 1,
					limit: 1,
					caseEnforced: true,
					tabIndex: i + 1,
					disableSpecialCharacters: true,
					disableSpace: true,
					onTextInputChange: onPasswordCharacterEntered
				});
				multitapfields.push(multitap);
			}
		}

		submitButton = popup.getPopupHtmlElement().getElementsByClassName("vdl_PopupButton")[0];
		updateInputFieldFocus();
	}

	function onPasswordCharacterEntered(inputElem) {
		allowBackspace = true;
		if (inputElem && inputElem.firstChild) {
			inputElem.classList.add("populated");
			inputElem.firstChild.style.visibility = 'hidden';
		}

		if (focusIndex < (config.fieldsLength - 1)) {
			focusIndex++;

			updateInputFieldFocus();
		} else {
			changeFocus("down");
		}
	}

	function changeFocus(direction) {
		switch (direction.toLowerCase()) {
			case "up":
				if (submitButton.classList.contains("focused")) {
					submitButton.classList.remove("focused");
					state = "multitap";
				}

				updateInputFieldFocus();
				if (focusIndex === (config.fieldsLength - 1) && multitapfields[focusIndex].getInput().length)
					deleteCharacter();
				break;

			case "down":
				if (state !== "doSubmit") {
					state = "doSubmit";
					multitapfields[focusIndex].getFullInput();
					blurInputElement();
					popup.setFocusTo(0);
				}
				break;
		}

	}

	function deleteCharacter(index) {
		if (isNaN(index)) index = focusIndex;
		if (multitapfields[index].getPasswordValue().length) {
			multitapfields[index].clearLast();

			var inputElement = document.getElementById('PasswordField' + index).getElementsByClassName('vm-multitap-input-text')[0];
			if (inputElement.classList.contains("populated"))
				inputElement.classList.remove("populated");
			return true;
		}

		return false;
	}

	function deleteOrPrevious() {
		//If the current field is not populated
		//move focus to previous and clear it
		if (!(deleteCharacter())) {
			if (focusIndex > 0) {
				focusIndex--;

				deleteCharacter();
			} else {
				if (closePopupCB) closePopupCB();
			}
		}

		updateInputFieldFocus();
	}

	function updateInputFieldFocus() {
		if (multitapfields.length) {
			for (var i = 0; i < config.fieldsLength; i++) {
				if (i === focusIndex) {
					multitapfields[i].showCursor();
					multitapfields[i].focus();
				} else {
					multitapfields[i].blur();
					multitapfields[i].hideCursor();
				}

				var inputElement = document.getElementById('PasswordField' + i);
				if (inputElement)
					focusInputElement(inputElement, (i === focusIndex));
			}
		}
	}

	function focusInputElement(elem, hasFocus) {
		if (hasFocus) {
			if (!(elem.classList.contains(cssInputFocus))) {
				elem.classList.add(cssInputFocus);
			}
		} else {
			if (elem.classList.contains(cssInputFocus)) {
				elem.classList.remove(cssInputFocus);
			}
		}
	}

	function blurInputElement() {
		var inputElement = document.getElementById('PasswordField' + focusIndex);
		if (inputElement.classList.contains(cssInputFocus)) {
			inputElement.classList.remove(cssInputFocus);
			multitapfields[focusIndex].blur();
		}
	}

	function onKey(e) {
		var key = e.keyCode;
		var keyCodes = vmDtvLib.keyCodes;

		switch (key) {

			case keyCodes.LEFT:
				if (state === "doSubmit" && closePopupCB) {
					popup.onKey(e);
				} else if (state === "errorPopup") {
					state = "multitap";
					passwordErrorPopup.onKey(e);
				} else if (allowBackspace) {
					deleteOrPrevious();
				}
				break;

			case keyCodes.UP:
				changeFocus("up");
				break;

			case keyCodes.DOWN:
				var password = getPassword();
				if (password.length === config.fieldsLength)
					changeFocus("down");
				break;

			case keyCodes.RIGHT:
			case keyCodes.OK:
				if (state === "doSubmit") {
					state = "validatePassword";
					popup.onKey(e);
				} else if (state === "errorPopup") {
					state = "multitap";
					passwordErrorPopup.onKey(e);
				}

				break;
			default:
				allowBackspace = false;
		}
	}

	function submitPassword() {
		if (vmDtvLib.tivoClient) {
			JCapsService.getInstance().submitPassword(
				pwidx.toString(),
				getPassword().toString(),
				onPasswordValidated,
				onFail
			);
		}
	}

	function getPassword() {
		var i = -1;
		var maxLength = config.fieldsLength;
		var passwordChars = [];
		while (++i < maxLength) passwordChars.push(multitapfields[i].getPasswordValue());

		return passwordChars;
	}

	function onPasswordValidated(data) {
		console.log("onPasswordValidated data.pwVerified", data.pwVerified);
		if (data.pwVerified) {
			onSuccess();
		} else {
			onFail(data);
		}
	}

	function onSuccess() {
		if (config.onPasswordValidated)
			config.onPasswordValidated();
	}

	function onFail(data) {
		state = "errorPopup";
		container.style.visibility = 'hidden';
		popup.hide();

		if (!passwordErrorPopup)
			passwordErrorPopup = new vmDtvLib.OptionPopup();

		var numRetries = data.retries;
		var errorPopupConfig = (numRetries > 0) ? getConfig("passwordRetryPopup") : getConfig("passwordLockedPopup");
		//		
		if (errorPopupConfig.body.indexOf("data.retries") > -1)
			errorPopupConfig.body = errorPopupConfig.body.split("data.retries").join(data.retries);

		passwordErrorPopup.init(errorPopupConfig)
			.addListener("CONTINUE", retry)
			.addListener("CANCEL", retry)
			.centre()
			.show();
		passwordErrorPopup.setFocusTo(0);
	}

	function retry(e) {
		state = "multitap";
		passwordErrorPopup.deletePopup();
		passwordErrorPopup = null;

		container.style.visibility = 'visible';
		//console.log("onFail:: container.style.visibility", container.style.visibility);
		popup.show();
		popup.reset();

		for (var i = 0; i < 3; i++) {
			console.log("multitapfields[i]", multitapfields[i]);
			deleteCharacter(i);
		}

		focusIndex = 0;
		updateInputFieldFocus();
		changeFocus("up");
	}

	/**
	 * create an option popup to submit password 
	 */
	function createPopup() {
		popup = new vmDtvLib.OptionPopup();
		var data = {
			id: "passwordPromptPopup",
			title: "We just need to check that…",
			body: "<p>you’re the account holder, or that you’ve got permission from the account holder to reset their PIN.</p>" +
				"<p>Tap in the " +
				"<em>" + getNumberAsPosition(pwidx[0]) + "</em>, " +
				"<em>" + getNumberAsPosition(pwidx[1]) + "</em>, and " +
				"<em>" + getNumberAsPosition(pwidx[2]) + "</em>" +
				" characters of your password into the boxes below, using the number buttons on your remote. " +
				"This password will be the one you use when you call our team.</p>" +
				"<footer>Forgotten you password? Don’t have one yet? Call 150 from your Virgin Media home phone " +
				"or 0345 454 1111 from any other phone.</footer>",
			idInitialFocus: "LOOPY",
			buttons: [{
				text: "Continue",
				id: "CONTINUE"
			}]
		};

		popup.init(data)
			.addListener("CONTINUE", onContinue)
			.addListener("CANCEL", closePopupCB)
			.show();
	}

	/**
	 * @private
	 * createContainer create the container element, which the components html template will be rendered into]
	 */
	function createContainer() {
		container = document.createElement('div');
		container.id = cssPasswordPrompt;
		container.setAttribute('tabindex', 0);
		container.classList.add(cssPasswordPrompt);

		if (config.parentID !== '') {
			var parent = document.getElementById(config.parentID);
			if (parent) {
				parent.appendChild(container);
			} else {
				document.body.appendChild(container);
			}
		} else {
			document.body.appendChild(container);
		}

	}

	function onContinue(e) {
		submitPassword();
	}

	this.init = init;
	this.onKey = onKey;
	this.remove = remove;
}
