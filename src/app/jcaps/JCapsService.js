function JCapsService() {
	/**
	 * Create a <script> tag in the head of the document, which has the url of the request to make to the selfcare API
	 * @param {Object} data object of key/value pairs to add as arguments to the request
	 * @param {Function} onsuccessCB A callback function if the request succeeds
	 * @param {Function} onErrorCB A callback function if the request fails
	 */
	function createRequest(data, onsuccessCB, onErrorCB) {
		var scriptElement = document.createElement("script");
		var head = document.getElementsByTagName("head")[0];
		head.appendChild(scriptElement);

		scriptElement.onload = function() {
			var products = window.getResponse();

			if (products.errorCode !== null) {
				if (onErrorCB) onErrorCB(products);
			} else {
				if (onsuccessCB) onsuccessCB(products);
			}

			head.removeChild(scriptElement);

			scriptElement = null;
		};

		var url = selfcareScriptUrl;

		var i = 0;

		for (var key in data) {
			url += (i === 0 ? '?' : '&') + (key + '=' + data[key]);
			i++;
		}

		scriptElement.src = url;
	}

	/**
	 * Send a call to JCAPS to get account details
	 * @param {func} onSuccessCB Callback function if succesfull
	 * @param {func} onErrorCB Callback function if NOT succesfull
	 */
	function getAccount(onSuccessCB, onErrorCB) {
		if (acct && pwIdxs) {
			onSuccessCB({
				acct: acct,
				pwIdxs: pwIdxs
			});

		} else if (vmDtvLib.tivoClient) {

			var data = {
				stbmac: getMacAddress(),
				action: "get_account",
				scid: getSCID(),
				pwidx: true
			};

			createRequest(
				data,
				//on success
				function(data) {
					onSuccessCB(data);
				},
				//on error
				onErrorCB
			);
		}
	}


	function submitPassword(positions, chars, onRequestSuccess, onRequestFail) {
		if (vmDtvLib.tivoClient) {
			var data = {
				stbmac: getMacAddress(),
				action: "check_password",
				acct: acct,
				positions: positions,
				chars: chars
			};

			createRequest(
				data,
				onRequestSuccess,
				onRequestFail
			);
		}
	}

	return {
		createRequest: createRequest,
		getAccount: getAccount,
		submitPassword: submitPassword
	};
}

/**
 * Get a singleton instance
 * @returns {JCapsService}
 */
JCapsService.getInstance = function() {
	if (!JCapsService.instance) JCapsService.instance = new JCapsService();

	return JCapsService.instance;
};

/**
 * A method to easly retrieve the Smart card ID, needed
 * for most JCAPS calls
 */
function getSCID() {
	//Without vmDtvLib.tivoClient this won't work
	if (!vmDtvLib.tivoClient) {
		//throw error
	} else {
		return vmDtvLib.tivoClient.getDevice().getConditionalAccessCardId();
	}
}

/**
 * A method to easly retrieve the mac addess, needed
 * for most JCAPS calls
 */
function getMacAddress() {
	//Without vmDtvLib.tivoClient this won't work
	if (!vmDtvLib.tivoClient) {
		//throw error
	} else {
		return vmDtvLib.tivoClient.getDevice().getMacAddress(window.tivo.client.NetworkInterfaceType.DOCSIS);
	}
}

var e = [];
var errorCode = null;
var acct = null;
var pwIdxs = [];
var pwVerified = false;
var retries = 0;

window.getResponse = function() {
	var data = {
		errorCode: errorCode,
		e: e,
		acct: acct,
		pwIdxs: pwIdxs,
		pwVerified: pwVerified,
		retries: retries
	};
	return data;
};
