var vmDtvLib = window.vmDtvLib;
var log = vmDtvLib.logger.getInstance("tvcare");
var loadScreen;
var VERSION = vmAppVersion.split("_")[0];
var DAX_NAME = 'tvcare';
var config;
var mainMenu;
var pinWin;
var passWin;
var tvCare;
var isPlugin = false;
var selfcareScriptUrl;
var errorPopup;

log.info("========= STARTING HELP APP ========= " + VERSION);


function doLoad() {

	loadScreen = vmDtvLib.loadingScreen.get();
	loadScreen.show();

	// Lib init callback
	function onLibInit(status, appConfig, tivoClient) {
		log.debug("=== appInit2, status: " + status, " status.ok: " + status.ok, " appConfig: " + appConfig);

		/* check for error return
		if (!status.ok) {
			log.error("Error from lib init", status.errMsg);
			//tivoClient.core.exit();
			return;
		}*/

		loadAppConfig(appConfig);
		vmDtvLib.appHistory.enter();
		tvCare = new TvcareService();
		loadMenu();
		setTimeout(function() { // TESTING ONLY .............
			loadScreen.hide();
		}, 1000);

	}

	log.debug("=== doLoad");
	vmDtvLib.lib.init(onLibInit);
}

function loadAppConfig(o) {
	//	log.debug('=== loadAppConfig : Help config : ' + o);
	if (!o || !o.tvCare) return;
	config = o.tvCare;
	selfcareScriptUrl = config.appConfig.selfcareScriptUrl;
	for (var prop in config.appConfig) log.debug("       ==> config " + prop + ": " + config.appConfig[prop]);
}

function loadMenu() {
	log.debug('=== loadMenu : ' + config.menu);

	// instantiate menus module, passing in config
	mainMenu = new Menus();
	mainMenu.init(config.menu, menuOnNavLimit, linkTo);

	attemptFocus();
	//	if (parent && parent.hasOwnProperty("onIframeReady"))
	//		parent.onIframeReady();
	//	else
	//		focusPlugin();
}

function attemptFocus() {
	if (parent && parent.hasOwnProperty("onIframeReady"))
		parent.onIframeReady();
	else
		focusPlugin();
}

/**
 * return focus to mainMenu if returning from parent
 */
function focusPlugin() {
	console.log("tvcare:: app focusPlugin");
	mainMenu.focusAndShow();
}

/**
 * remove focus from mainMenu if returning to parent
 */
function blurPlugin() {
	console.log("tvcare:: app blur");
	mainMenu.hideAndBlur();
}

/**
 * Play a tivo sound
 * @param {String} id
 */
function playSound(id) {
	if (vmDtvLib.hasOwnProperty("sound"))
		vmDtvLib.sound(id);
}


/**
 * onNavLimit handler for the menu component - handles navigation between components
 * 
 * @param event {KeyEvent} - key event triggering this callback
 */
function menuOnNavLimit(event) {
	log.debug('=== menuOnNavLimit .......... event: ' + event);
	var key = event.keyCode;
	var keyCodes = vmDtvLib.keyCodes;

	if (key === keyCodes.UP) {
		if (parent && parent.hasOwnProperty("onNavLimit")) {
			parent.onNavLimit(event);
			blur();
		}

	} else if (key === keyCodes.LEFT) {
		//Go back to parent app and load menu (should still be loaded)
		//linkTo('back'); // back up history stack to another view or to previous app
		if (parent && parent.hasOwnProperty("closePlugin"))
			parent.closePlugin();
		else if (hasOwnProperty("linkTo"))
			linkTo('back');

		playSound("UP_DOWN");
		//log.debug("* article on nav limit. left");

	} else if (key === keyCodes.DOWN) {
		// return false;
		playSound("BONK");
	}
}

function linkTo(linkStr) {
	log.debug('=== linkTo : linkStr: ' + linkStr);

	var link = linkStr.split(':');

	switch (link[0]) { // types: menu/external(back)/article/search
		case 'back':
			// go back up history stack or exit app via appHistory [back]
			break;
		case 'menu':
			// show menus & focus the specified menu entry [menu:col,row1,row2,row3]
			mainMenu.setState(link[1]);
			break;
		case 'search':
			// load menus (if necessary) focus & start search with specified searchTerm [search:term]
			break;
		case 'resetpin':
		case 'refresh':
			initiateTVCareService(link[0]);
			//                        showPinPrompt(link[0]);
			break;
		case 'ext': // external link [ext:link]
			// use appHistory to load the next app
			break;
		default:
			// link to an article [articleName]
	}

	// if an external link, use appHistory to load new app
	// else if article, load article structure and specific content
	// else load menus and show appropriate menu/search
}

/**
 * In order to perform tv care services the account details are
 * required. Request account details from JCAPS
 * @param {type} link
 * @returns {undefined} */
function initiateTVCareService(link) {
	JCapsService.getInstance().getAccount(
		//on success
		function(data) {
			acct = data.acct;
			//pwidx = data.pwIdxs;
			console.log("on account details success acct", acct);
			if (link === 'resetpin')
				showPasswordPrompt(link, data.pwIdxs);
			else
				loadTVCareService(link);
		},
		//on error retrieving account

		function(data) {
			console.log("Error retrieving account details: data", data);
			//show error popup
			showErrorPopup("accountError");
		}
	);
}

/**
* Show the password prompt, if required, once acct details have been retrieved

 * @param {type} link
 * @param {type} pwIdxs
 * @returns {undefined} */
function showPasswordPrompt(link, pwIdxs) {
	mainMenu.blur();
	passWin = new PasswordPrompt(removePasswordPrompt);

	var resultCallBackFunc = function() {
		console.log("resultCallBackFunc");
		removePasswordPrompt();
		loadTVCareService(link); // boolean result
	};

	passWin.init(
		//PasswordPrompt settings
		{
			onPasswordValidated: resultCallBackFunc
		},
		//indexes to validate
		pwIdxs);

	document.addEventListener("keydown", passWin.onKey, false);

	changeDepth();
}

function removePasswordPrompt() {
	if (passWin) {
		document.removeEventListener("keydown", passWin.onKey, false);
		passWin.remove();
		passWin = null;

		attemptFocus();
		//		if (parent && parent.hasOwnProperty("onIframeReady"))
		//			parent.onIframeReady();
		//		else
		//			focusPlugin();
	}

	resetDepth();
}

function loadTVCareService(link) {
	resetDepth();

	if (passWin) document.removeEventListener("keydown", passWin.onKey, false);

	tvCare.handleRequest(link).

	success(function(data) {
		handleTVCareResponse(data);
	}).
	error(function(data) {
		handleTVCareError(data);
	});
}

/**
 * This is called if the requested task has been added and is not avaiable for 24 hours
 * @param {type} taskId
 * @returns {o.tvCare.success} */
function getResponseMessage(taskId) {
	var timeSinceTaskAttempt = tvCare.getTimeSinceLastAttempt(taskId);
	if (config[taskId] && !isNaN(timeSinceTaskAttempt)) {
		if (timeSinceTaskAttempt < 15) {
			return config[taskId]["success"];
		} else {
			return config[taskId]["alreadyDone"] || config[taskId]["success"];
		}
	} else {
		console.log("Erm, something wrong here. Could not find a registered task for " + taskId);
		return ("Erm, something wrong here. Could not find a registered task for " + taskId);
	}
}

function showErrorPopup(id) {
	if (!errorPopup)
		errorPopup = new vmDtvLib.OptionPopup();

	var popupConfig = config[id];

	var errorPopupConfig = {
		id: "errorPopup",
		title: popupConfig.title,
		body: popupConfig.body,
		buttons: popupConfig.buttons
	};

	errorPopup.init(errorPopupConfig)
		.addListener("CONTINUE", closeErrorPopup)
		.addListener("CANCEL", closeErrorPopup)
		.centre()
		.show();
	errorPopup.setFocusTo(0);

	changeDepth();

	if (isPlugin)
		window.onKey = errorPopup.onKey;
	else
		document.addEventListener('keydown', errorPopup.onKey, false);
}

function closeErrorPopup() {
	if (!isPlugin)
		document.removeEventListener('keydown', errorPopup.onKey, false);

	errorPopup.deletePopup();
	errorPopup = null;
	attemptFocus();
	resetDepth();
}

function isOptionAvailable(optionData) {
	if (tvCare)
		return (tvCare.isOptionAvailable(optionData));

	//If tvCare hasn't been created
	return false;
}

function handleTVCareResponse(response) {
	mainMenu.focusAndShow();
}

function handleTVCareError(response) {
	console.log("TV Care request success!", response);
}

function changeDepth() {
	if (parent.hasOwnProperty("getParentHTMLElement")) {
		var htmlElem = parent.getParentHTMLElement();

		if (!(htmlElem.classList.contains("focused")))
			htmlElem.classList.add("focused");
	}
}

function resetDepth() {
	if (parent.hasOwnProperty("getParentHTMLElement")) {
		var htmlElem = parent.getParentHTMLElement();

		if (htmlElem.classList.contains("focused"))
			htmlElem.classList.remove("focused");
	}
}

function getConfig(id) {
	if (id && config[id])
		return config[id];

	return config;
}
