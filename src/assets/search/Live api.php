<?php
   /*
	* TiVo Help Application - API Script
	* @author Jonathan Walmsley <jonathan.walmsley@virginmedia.co.uk>
	*/

//turn all php errors into exceptions - unless the exception is explicitly caught the script will terminate 
set_error_handler('onError', E_ALL);
// add a handler for un caught exceptions
set_exception_handler('onUncaughtException');

		/*
		 * Loading home menu or searching second level menus
		 */
		header('Content-type: application/xml');
		//$doc = simplexml_load_file('../xml/helpHome.xml'); // for development
		$doc = simplexml_load_file('../../../../publishConfig/apps/help/live/xml/helpHome.xml'); // for release - must use a relative path to workaround domain name issues
		//$doc = simplexml_load_file('../../../../publishConfig/HelpApp/xml/helpHome.xml');	// one-off for the Labs - must use a relative path to workaround domain name issues
		if (isset($_GET['s'])) {
			// Search string specified.
			$searchString = strtolower($_GET['s']);
			$out = new SimpleXMLElement('<?xml version="1.0" ?><tivoHelp/>');
			$level1Idx = 0;
			foreach ($doc->child as $child) {
				$level2Idx = 0;
				foreach ($child->child as $topic) { // Level 2 titles
					$startIdx = strpos(strtolower($topic->displayTitle), $searchString); // add 'unescape' [htmlspecialchars_decode] (to turn &amp; back into & etc) around $topic->displayTitle
					//strpos returns false if searchString is not found
					if ($startIdx !== false) {						// must be !== to avoid catching value of zero
						checkResult( $topic, $startIdx, $level1Idx, $level2Idx, null );
					}
					if ($topic->child) {			// Search Level 3 if there is one
						$level3Idx = 0;
						foreach($topic->child as $subtopic) {
							$startIdx = strpos(strtolower($subtopic->displayTitle), $searchString);
							if ($startIdx !== false) {
								checkResult( $subtopic, $startIdx, $level1Idx, $level2Idx, $level3Idx );
							}
							$level3Idx++;
						}
					}
					$level2Idx++;
				}
				$level1Idx++;
			}
			flushResults();
			echo $out->asXML();
		} else {
			// No search string specified. Return entire tree,
			echo $doc->asXML();
		}

$resultUrls=array();

function checkResult( $elem, $matchIdx, $idx1, $idx2, $idx3 ) {
	global $resultUrls;		// use global array
	$arr = array( $elem, $matchIdx, $idx1, $idx2, $idx3 );
	if (isset($elem->uri)) {
		$uri_str = (string)$elem->uri;
		if (!$resultUrls) $resultUrls=array();
		$prev_elem = array_key_exists($uri_str,$resultUrls) ? $resultUrls[$uri_str][0] : null;
		if ($prev_elem) {
			$prev_elem_weight = 0 + ($prev_elem->weighting ? $prev_elem->weighting : 0);
			$this_elem_weight = 0 + ($elem->weighting ? $elem->weighting : 0);
			if ($this_elem_weight > $prev_elem_weight) {
				$resultUrls[$uri_str] = $arr;		// overwrite previous entry with this one
			}
		} else $resultUrls[$uri_str] = $arr;		// no previous entry with this URL, so add as a new entry
	} else {
		$resultUrls[] = $arr;						// no URL, so add as a new entry
	}
}

function flushResults() {
	global $resultUrls;		// use global array
	if (!$resultUrls) $resultUrls=array();
	foreach ($resultUrls as $res) {
		addResult( $res[0],$res[1],$res[2],$res[3],$res[4] );
	}
}

function addResult( $elem, $matchIdx, $idx1, $idx2, $idx3 ) {
	global $out;		// use global var
	$newChild = $out->addChild('child');
	if ($idx3 !== null) {
		$newChild->addChild('level3Idx', $idx3);
		$newChild->addChild('displayTitle', htmlspecialchars($elem->displayTitle));
	} else {
		$newChild->addChild('displayTitle', htmlspecialchars($elem->displayTitle));
	}
	$newChild->addChild('matchPos', $matchIdx);
	$newChild->addChild('uri', $elem->uri);
	$newChild->addChild('level1Idx', $idx1);
	$newChild->addChild('level2Idx', $idx2);
	if ($elem->weighting) {
		$newChild->addChild('weighting', $elem->weighting);		// Add default of zero here (in else) ???
	}
	if ($elem->intro) {
		$newChild->intro = $elem->intro;
	}
	// Add 3rd level if there is one
	if ($elem->child) { // Level 3 Titles
		foreach($elem->child as $subtopic) {
			$newChildChild = $newChild->addChild('child');
			$newChildChild->addChild('displayTitle', htmlspecialchars($subtopic->displayTitle));
			$newChildChild->addChild('uri', $subtopic->uri);
		}
	}
}

/*private*/function onError($errNo, $message, $fileName, $lineNo) {
	//echo "onError $errNo, $message, $fileName, $lineNo\n";
	throw new ErrorException($message, $errNo, 0, $fileName, $lineNo);
}

/*private*/function onUncaughtException($exception) {
	$message = ' File:' . $exception->getFile() . ' Line:' . $exception->getLine() . ' ' . $exception->getMessage();
	echo 'onUncaughtException .... ' . $message;
}

?>