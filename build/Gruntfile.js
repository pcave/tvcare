/*
ng-boilerplate
--------------
This grunt file and other build process files are based on ng-boilerplate
which is copyright as follows: 

Copyright (c) 2013 Josh David Miller <josh@joshdmiller.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */


module.exports = function(grunt) {

	/** 
	 * Load required Grunt tasks. These are installed based on the versions listed
	 * in `package.json` when you do `npm install` in this directory.
	 */
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-bump');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-html2js');
	grunt.loadNpmTasks('grunt-jsbeautifier');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-compress');

	var path = require('path');
	var util = require('util');

	var BASE62_CHARACTER_SET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    var encode2Base62 = function(integer){
        if (integer === 0) {return '0';}
        var s = '';
        while (integer > 0) {
            s = BASE62_CHARACTER_SET[integer % 62] + s;
            integer = Math.floor(integer/62);
        }
        return s;
    };


	/**
	 * Load in our build configuration file.
	 *
	 * The config file name can be specified as --config option on the command line, defaults to build
	 * The config is always picked up from the config subdirectory and has extension .config.js , its just the name that can be specified
	 * If a different config is specified, that config should specify different dev and live directories where the build is produced, so those produced by the build.config.js file are not overwritten 
	 */
	var userConfig = require('./config/' +  (grunt.option('config') || 'build') + '.config.js');

	/**
	 * Load package config file early in order to add some logic inside the taskConfig object structure
	 */
	//var pkgFile = grunt.file.readJSON("build/config/package.json");
	var pkgFile = grunt.file.readJSON("package.json");
	if (pkgFile.angular) {
		console.log("+++++++++ ANGULAR +++++++++");
	} else {
		console.log("+++++++++ NOT ANGULAR +++++++++");
	}
	// replace dots so 1.0.0 becomes 1-0-0 for use in filenames
	pkgFile.version = pkgFile.version.replace(/\./g, '-');

	// add a unique build id. number of seconds since 1/1/15 in base 62
	var rawBuildId = Math.floor( ((new Date()).getTime()-(new Date(2015, 0, 1)).getTime())/1000 );
	var buildId = ("0" + encode2Base62(rawBuildId)).slice(-5);

	// read in file(s) for minification
	var minFiles = grunt.option('files') || '';

	// constants
	var concatFileName = pkgFile.name + (userConfig.addVersionToFilenames? '_' + pkgFile.version: '');


	/**
	 * This is the configuration object Grunt uses to give each plugin its
	 * instructions.
	 */
	var taskConfig = {
		/**
		 * We read in our `package.json` file so we can access the package name and
		 * version. It's already there, so we don't repeat ourselves here.
		 */
		pkg: pkgFile,

		/**
		 * The banner is the comment that is placed at the top of our compiled
		 * source files. It is first processed as a Grunt template, where the `<%=`
		 * pairs are evaluated based on this very configuration object.
		 */
		meta: {
			banner: '/**\n' +
				' * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
				(pkgFile.homepage ? ' * <%= pkg.homepage %>\n' : '') +
				' *\n' +
				' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
				(pkgFile.licenses ? ' * Licensed <%= pkg.licenses.type %> <<%= pkg.licenses.url %>>\n' : '') +
				' */\n'
		},

		/**
		 * Increments the version number, etc.
		 */
		bump: {
			options: {
				files: [
					"package.json",
					"bower.json"
				],
				commit: false,
				commitMessage: 'chore(release): v%VERSION%',
				commitFiles: [
					"package.json",
					"client/bower.json"
				],
				createTag: false,
				tagName: 'v%VERSION%',
				tagMessage: 'Version %VERSION%',
				push: false,
				pushTo: 'origin'
			}
		},

		/**
		 * The directories to delete when `grunt clean` is executed.
		 */
		clean: {
			build_dirs: [
				'<%= build_dir %>',
				'<%= compile_dir %>'
			],
			temp_dirs: [
				'<%= tempcss_dir %>'
			]
		},

		/**
		 * The `copy` task just copies files from A to B. We use it here to copy
		 * our project assets (images, fonts, etc.) and javascripts into
		 * `build_dir`, and then to copy the assets to `compile_dir`.
		 */
		copy: {
			src_js_pre_beautify: {
				files: [{
					src: ['src/**/*.js'],
					dest: 'backup_js_pre_beautify',
					cwd: '.',
					expand: true
				}]
			},
			build_app_assets: {
				files: [{
					src: ['**'],
					dest: '<%= build_dir %>/assets/',
					cwd: 'src/assets',
					expand: true
				},{ // copy across app assets which are not in src/assets/
					src: ['<%= app_files.assets %>'],
					dest: '<%= build_dir %>/assets/',
					cwd: '.',
					expand: true,
					// the src file paths start with src/ , we need to remove this so when they are placed in assets they are not placed in a src sub directory
					// the rename function is called for each file and returns the destination to use
					rename: function(dest, src){
						//console.log("dest:"+dest+" src:"+src);
						return dest + src.replace(/^src\//, '');
					}
				}]
			},
			build_lib_assets: {
				files: [{
					src: ['<%= library_files.assets %>'],
					dest: '<%= build_dir %>/assets/',
					cwd: '.',
					expand: true,
					// the src file paths start with src/lib/ , we need to remove this so when they are placed in assets they are not placed in a src/lib sub directory
					// the rename function is called for each file and returns the destination to use
					rename: function(dest, src){
						//console.log("dest:"+dest+" src:"+src);
						return dest + src.replace(/^src\/lib\//, '');
					}					
				}]
			},
			build_vendor_assets: {
				files: [{
					src: ['<%= vendor_files.assets %>'],
					dest: '<%= build_dir %>/assets/',
					cwd: '.',
					expand: true,
					flatten: true
				}]
			},
			build_appjs: {
				files: [{
					src: ['<%= app_files.js %>'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			build_vendorjs: {
				files: [{
					src: ['<%= vendor_files.js %>'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			build_css_temp: {
				filename: '<%= tempcss_dir %>/<%= app_files.scss %>',
				files: [{
					src: ['src/**/*', '!src/**/*.js'],
					dest: '<%= tempcss_dir %>',
					cwd: '.',
					expand: true
				}]
			},
			build_pubConf: {
				files: [{
					src: ['publishConfig/**', '!publishConfig/**/*.bak'],
					dest: '<%= build_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			compile_pubConf: {
				files: [{
					src: ['publishConfig_Release/**', '!publishConfig_Release/**/*.bak'],
					dest: '<%= compile_dir %>/',
					cwd: '.',
					expand: true
				}]
			},
			compile_assets: {
				files: [{
					src: ['**', '!**/*.css'], // do not copy css file - sass:compile handles this
					dest: '<%= compile_dir %>/assets',
					cwd: '<%= build_dir %>/assets',
					expand: true
				}]
			},
			icon: {
				files: [{
					src: ['icon_<%= pkg.name %>.png'],
					dest: '<%= compile_dir %>/release/',
					cwd: 'src',
					expand: true
				}]
			}
		},

		htmlTemplates: {
			options: {
				prefix_lib: 'lib_',
				prefix_anglib: 'alib_',
				libPattern: /dtv-html-codelib/,
				anglibPattern: /dtv-html-angCodelib/
			},
			build: {
				src: ['<%= app_files.tmpl %>'],
				dest: '<%= build_dir %>',
				path: '<%= build_dir %>/<%= templates.html %>.js'
			}
		},

		/**
		 * `grunt concat` concatenates multiple source files into a single file.
		 */
		concat: {
			/**
			 * The `build_css` target concatenates compiled CSS and vendor CSS
			 * together.
			 */
			build_css: {
				src: [
					'<%= vendor_files.css %>',
					'<%= sass.build.dest %>'
				],
				dest: '<%= sass.build.dest %>'
			},

			/**
			 * The `compile_js` target is the concatenation of our application source
			 * code and all specified vendor source code into a single file.
			 */
			compile_js: {
				options: {
					banner: '<%= meta.banner %>'
				},
				src: [
					'<%= vendor_files.js %>',
					'<%= module.prefix %>',
					'<%= app_files.js %>',
					'<%= html2js.app.dest %>',
					'<%= html2js.lib.dest %>',
					'<%= module.suffix %>',
					'<%= htmlTemplates.build.path %>'
				],
				dest: '<%= compile_dir %>/' + concatFileName + '.js'
			}
		},

		/**
		 * Minify the sources!
		 */
		uglify: {
			compile: {
				options: {
					banner: '<%= meta.banner %>'
				},
				files: {
					'<%= concat.compile_js.dest %>': '<%= concat.compile_js.dest %>'
				}
			},
			files: {
				files: {
					'bin/min/files_min.js': minFiles
				}
			}
		},

		/**
		 * `sass` compiles and uglifies SCSS files into CSS.
		 * Only our `main.scss` file is included in compilation;
		 * all other files must be imported from this file.
		 */
		sass: {
			build: {
				filename: concatFileName + '.css',
				src: ['<%= copy.build_css_temp.filename %>'],
				dest: '<%= build_dir %>/assets/<%= sass.build.filename %>'
			},
			compile: {
				options: {
					outputStyle: 'compressed'
				},
				src: ['<%= sass.build.dest %>'],
				dest: '<%= compile_dir %>/assets/<%= sass.build.filename %>'
			}
		},


		/**
		 * `jshint` defines the rules of our linter as well as which files we
		 * should check. This file, all javascript sources, and all our unit tests
		 * are linted based on the policies listed in `options`. But we can also
		 * specify exclusionary patterns by prefixing them with an exclamation
		 * point (!); this is useful when code comes from a third party but is
		 * nonetheless inside `src/`.
		 */
		jshint: {
			src: [
				'<%= app_files.js %>',
				'!**/dax.js'
			],
			test: [
				'<%= app_files.specjs %>'
			],
			gruntfile: [
				'build/Gruntfile.js'
			],
			options: {
				//curly: true,
				immed: true,
				newcap: true,
				noarg: true,
				sub: true,
				//boss: true,
				eqnull: true
			},
			globals: {}
		},

		/**
		 * HTML2JS is a Grunt plugin that takes all of your template files and
		 * places them into JavaScript files as strings that are added to
		 * AngularJS's template cache. This means that the templates too become
		 * part of the initial payload as one JavaScript file. Neat!
		 */
		html2js: {
			/**
			 * These are the templates from `src/app`.
			 */
			app: {
				options: {
					base: 'src/app',
					module: '<%= templates.ang_app %>' // renames the Angular module, defaults to templates-<subtask_name> ie templates-app
				},
				src: ['<%= app_files.atpl %>'],
				dest: '<%= build_dir %>/<%= templates.ang_app %>.js'
			},

			/**
			 * These are the templates from `src/lib`.
			 */
			lib: {
				options: {
					base: 'src/lib',
					module: '<%= templates.ang_lib %>'
				},
				src: ['<%= app_files.ctpl %>'],
				dest: '<%= build_dir %>/<%= templates.ang_lib %>.js'
			},

			/**
			 * For non-Angular projects - do nothing.
			 */
			nonAng: {}
		},


		/**
		 * The Karma configurations.
		 */
		karma: {
			options: {
				configFile: '<%= build_dir %>/karma-unit.js'
			},
			unit: {
				runnerPort: 9101,
				background: true
			},
			continuous: {
				singleRun: true
			}
		},

		/**
		 * The `index` task compiles the `app.html` file as a Grunt template. CSS
		 * and JS files co-exist here but they get split apart later.
		 */
		index: {

			/**
			 * During development, we don't want to have wait for compilation,
			 * concatenation, minification, etc. So to avoid these steps, we simply
			 * add all script files directly to the `<head>` of `app.html`. The
			 * `src` property contains the list of included files.
			 */
			build: {
				files: [
				{
					dest: '<%= build_dir %>',
					src: [
						'<%= app_files.html %>',
						'<%= vendor_files.js %>',
						//'<%= build_dir %>/src/**/*.js',
						'<%= app_files.js %>',
						'<%= html2js.lib.dest %>',
						'<%= html2js.app.dest %>',
						'<%= vendor_files.css %>',
						'<%= sass.build.dest %>',
						'<%= htmlTemplates.build.path %>'
					]
				}, {
					dest: '<%= build_dir %>/' + pkgFile.name + '.html',
					src: '<%= app_files.mainHtml %>'
				}

				]
			},

			/**
			 * When it is time to have a completely compiled application, we can
			 * alter the above to include only a single JavaScript and a single CSS
			 * file. Now we're back!
			 */
			compile: {
				files: [
				{
					dest: '<%= compile_dir %>',
						src: [
					'<%= app_files.html %>',
							'<%= concat.compile_js.dest %>',
							'<%= vendor_files.css %>',
							'<%= sass.compile.dest %>'
						]
				}, {
					dest: '<%= compile_dir %>/' + pkgFile.name + '.html',
					src: '<%= app_files.mainHtml %>'
				}
				]
			}
		},

		/**
		 * This task compiles the karma template so that changes to its file array
		 * don't have to be managed manually.
		 */
		karmaconfig: {
			unit: {
				dir: '<%= build_dir %>',
				src: [
					'<%= vendor_files.js %>',
					'<%= test_files.js %>', // must come here, is the angular mocks code
					'<%= app_files.jsunit %>', //all app js files including test files needed for unit testing
					'<%= html2js.app.dest %>',
					'<%= html2js.lib.dest %>'
				]
			}
		},

		/**
		 * And for rapid development, we have a watch set up that checks to see if
		 * any of the files listed below change, and then to execute the listed
		 * tasks when they do. This just saves us from having to type "grunt" into
		 * the command-line every time we want to see what we're working on; we can
		 * instead just leave "grunt watch" running in a background terminal. Set it
		 * and forget it, as Ron Popeil used to tell us.
		 *
		 * But we don't need the same thing to happen for all the files.
		 */
		delta: {
			/**
			 * By default, we want the Live Reload to work for all tasks; this is
			 * overridden in some tasks (like this file) where browser resources are
			 * unaffected. It runs by default on port 35729, which your browser
			 * plugin should auto-detect.
			 */
			options: {
				livereload: true
			},

			/**
			 * When the Gruntfile changes, we just want to lint it. In fact, when
			 * your Gruntfile changes, it will automatically be reloaded!
			 */
			buildfiles: {
				files: ['build/Gruntfile.js', 'build/config/build.config.js'],
				tasks: ['build'],
				options: {
					livereload: false
				}
			},

			/**
			 * When our JavaScript source files change, we want to beautify & lint them,
			 * run our unit tests, copy js files and rebuild index for live update whilst developing.
			 */
			jssrc: {
				files: ['<%= app_files.js %>'],
				tasks: ['beautifyjs', 'jshint:src', 'copy:build_appjs', 'index:build'] // 'karma:unit:run', 
			},

			/**
			 * When assets are changed, copy them. Note that this will *not* copy new
			 * files, so this is probably not very useful.
			 */
			assets: {
				files: ['src/assets/**/*', '<%= library_files.assets %>'],
				tasks: ['copy:build_app_assets', 'copy:build_lib_assets']
			},

			/**
			 * When app.html changes, we need to compile it for development.
			 */
			html: {
				files: ['<%= app_files.html %>'],
				tasks: ['index:build']
			},

			/**
			 * When our Angular templates change, we rewrite the Angular template cache
			 * and rebuild index for live update whilst developing.
			 */
			tpls: {
				files: [
					'<%= app_files.atpl %>',
					'<%= app_files.ctpl %>'
				],
				tasks: ['html2js', 'index:build']
			},

			/**
			 * When our tmpl templates change, we recreate the html template
			 * and rebuild index for live update whilst developing.
			 */
			tmpls: {
				files: [
					'<%= app_files.tmpl %>'
				],
				tasks: ['htmlTemplates', 'index:build']
			},

			/**
			 * When the CSS files change, we need to compile them and rebuild index for live update whilst developing.
			 */
			sass: {
				files: ['src/**/*.scss'],
				tasks: ['clean:temp_dirs', 'copy:build_css_temp', 'cssUrls', 'sass:build', 'index:build']
			},

			/**
			 * When a JavaScript unit test file changes, we only want to lint it and
			 * run the unit tests. We don't want to do any live reloading.
			 */
			jsunit: {
				files: [
					'<%= app_files.specjs %>'
				],
				tasks: ['jshint:test', 'karma:unit:run'],
				options: {
					livereload: false
				}
			},

			/**
			 * When publishConfig files change, we copy them over to dev/publishConfig
			 */
			pubConf: {
				files: ['publishConfig/**'],
				tasks: ['copy:build_pubConf']
			}

		},

		/**
		 * Beautify all JavaScript files inside the src/ folder using the rules below.
		 * Beautified files replace originals, so we backup the originals first using
		 * copy:src_js_pre_beautify
		 * Found that using jsbeautifier on CSS and HTML files makes unwanted changes.
		 */
		"jsbeautifier": {
			options: {
				js: {
					braceStyle: "collapse",
					breakChainedMethods: false,
					e4x: false,
					evalCode: false,
					indentChar: " ",
					indentLevel: 0,
					indentSize: 4,
					indentWithTabs: true,
					jslintHappy: false,
					keepArrayIndentation: false,
					keepFunctionIndentation: false,
					maxPreserveNewlines: 3,
					preserveNewlines: true,
					spaceBeforeConditional: true,
					spaceInParen: false,
					unescapeStrings: false,
					wrapLineLength: 0
				},
				css: {
					indentChar: "	",
					indentSize: 1
				}
			},
			"build": {
				src: ["src/**/*.js"],
				options: {}
			},
			"make_readable": {
				src: ["bin/**/*.js", "bin/**/*.css"],
				options: {
					dest: "bin/readable"
				}
			}
		},

		/**
		 * Start the grunt server to serve files from the build or bin directories.
		 * Just point the browser to http://localhost:9000 or http://localhost:9001
		 * When using `bin` the server continues running with `keepalive`, this
		 * does mean, however, that subsequent tasks are not performed.
		 * Now using v0.9 so we can change the default 'index.html' to '<appName>.html'.
		 */
		connect: {
			build: {
				options: {
					port: 9000,
					hostname: '*',
					livereload: 35729,
					base: {
						path: '<%= build_dir %>',
						options: {
							index: '<%= pkg.name %>.html'
						}
					}
				},
				livereload: {}
			},
			bin: {
				options: {
					port: 9001,
					hostname: '*',
					livereload: 35729,
					keepalive: true,
					base: {
						path: '<%= compile_dir %>',
						options: {
							index: '<%= pkg.name %>.html'
						}
					}
				},
				livereload: {}
			}
		},

		/**
		 * Zip App or PublishConfig content for release.
		 */
		compress: {
			app: {
				options: {
					archive: 'bin/release/' + concatFileName + '.zip'
				},
				expand: true,
				cwd: 'bin/',
				src: ['<%= pkg.name %>.html', concatFileName + '.js', 'assets/**/*'],
				dest: 'htmlApps/<%= pkg.name %>/<%= pkg.version %>/' // prepends files with release dir structure
			},
			publishConfig: {
				options: {
					archive: 'bin/release/publishConfig_' + concatFileName + '.zip'
				},
				expand: true,
				cwd: 'publishConfig_Release/',
				src: ['**/*', '!**/*.bak'],
				dest: 'publishConfig/htmlApps/<%= pkg.name %>/<%= publishConfig.versionName %>/'
			}
		},

		/**
		 * Grunt task to make CSS URLs relative to a main CSS file with @import rules.
		 * See cssUrls multi-task below.
		 */
		cssUrls: {
			/* src *(required)*: The file location of the css with the @import rules. */
			options: {
				libPattern: /^\.\.\/lib\//, // scss files whose import path matches this are library files
				libRoot: '<%= tempcss_dir %>/src/lib', // url references in scss library files will be resolved to be a url relative to this base url

				vendorPattern: /^\.\.\/vendor\//, // scss files whose import path matches this are vendor files
				vendorRoot: '<%= tempcss_dir %>', //  url references in scss vendor files will be resolved to be a url relative to this base url
				// scss files referenced in @imports that are not library or vendor are classed as other
				otherAssetUrlAssetPattern: /\.\.\/assets\//, // for scss files classed as other, urls in these files that match this pattern are to items in the assets directory 
				otherAssetRoot: '<%= tempcss_dir %>/src/assets', // resolve urls to items in assets directory relative to this base
				otherOtherRoot: '<%= tempcss_dir %>/src'  // resolve all other urls in scss files relative to this base
			},
			src: '<%= copy.build_css_temp.filename %>'
		}

	};

	grunt.initConfig(grunt.util._.extend(taskConfig, userConfig));

	/**
	 * In order to make it safe to just compile or copy *only* what was changed,
	 * we need to ensure we are starting from a clean, fresh build. So we rename
	 * the `watch` task to `delta` (that's why the configuration var above is
	 * `delta`) and then add a new task called `watch` that does a clean build
	 * before watching for changes.
	 */
	grunt.renameTask('watch', 'delta');
	grunt.registerTask('watch', ['build', 'karma:unit', 'delta']);

	/**
	 * The default task is to build and compile.
	 */
	grunt.registerTask('default', ['build', 'compile']);

	/**
	 * The `build` task gets your app ready to run for development and testing.
	 */
	grunt.registerTask('build', [
		'beautifyjs', 'clean', (pkgFile.angular ? 'html2js' : 'html2js:nonAng'), 'jshint', 'copy:build_css_temp', 'cssUrls', 'sass:build',
		'concat:build_css', 'copy:build_app_assets', 'copy:build_lib_assets', 'copy:build_vendor_assets', 'htmlTemplates',
		'copy:build_appjs', 'copy:build_vendorjs', 'copy:build_pubConf', 'index:build', 'karmaconfig' //, 'karma:continuous' 
	]);

	/**
	 * The `compile` task gets your app ready for deployment by concatenating and
	 * minifying your code.
	 */
	grunt.registerTask('compile', [
		'sass:compile', 'copy:compile_assets', 'concat:compile_js', 'uglify:compile', 'index:compile',
		'jsbeautifier:make_readable', 'copy:compile_pubConf', 'copy:icon', 'compress'
	]);

	/**
	 * The `beautifyjs` task beautifies all JavaScript files inside the src/ folder.
	 * Beautified files replace originals, so we backup the originals first using
	 * copy:src_js_pre_beautify
	 */
	grunt.registerTask('beautifyjs', ['copy:src_js_pre_beautify', 'jsbeautifier:build']);

	/**
	 * The `serve` task starts the grunt server and proceeds to `watch`.
	 * The server runs whilst `watch` runs.
	 */
	grunt.registerTask('serve', ['connect:build', 'watch']);
	grunt.registerTask('servedev', ['connect:build', 'delta']);

	/**
	 * The `liveserve` task compiles the code and starts the grunt server.
	 * The server continues to run with `keepalive`, so ensure `connect` is the last task.
	 */
	grunt.registerTask('liveserve', ['compile', 'connect:bin']);
	grunt.registerTask('servebin', ['connect:bin', 'delta']);


	/**
	 * The `filesmin` task is a stand-alone task to minify files specified on the command line.
	 * Use syntax :  rgrunt filesmin --files=path/to/files
	 * For example:  rgrunt filesmin --files=src/services/*.js
	 */
	grunt.registerTask('filesmin', ['uglify:files']);


	/**
	 * A utility function to get all app JavaScript sources.
	 */
	function filterForJS(files) {
		return files.filter(function(file) {
			return file.match(/\.js$/);
		});
	}

	/**
	 * A utility function to get all app HTML sources.
	 */
	function filterForHtml ( files ) {
	return files.filter( function ( file ) {
		return file.match( /\.html$/ );
	});
	}

	/**
	 * A utility function to get all app CSS sources.
	 */
	function filterForCSS(files) {
		return files.filter(function(file) {
			return file.match(/\.css$/);
		});
	}


	/**
	 * We want to fetch all the template files (*.tmpl.html), read in their contents,
	 * assign them to a variable (created from the filename) and load them into a separate
	 * js file. This file gets packaged up with the rest of the js files and the object vars
	 * will be available to the app code.
	 */
	grunt.registerMultiTask('htmlTemplates', 'Process template files', function() {
		//console.log("------>> this.filesSrc: " + this.filesSrc);	// String of filepaths; comma separated
		var options = this.options({});

		if (this.filesSrc.length === 0) {
			console.log("------>> No (*.tmpl.html) templates found");
			return;
		}
		var fileContents = '';
		var fileList = (this.filesSrc + "").split(',');
		fileList.forEach(function(file) {

			console.log("------>> found template: " + file);
			var propName = file.toString().replace(/\.tmpl\.html$/, ''); // Take the filepath and use for the property name, removing the .tmpl.html file ending.
			propName = propName.replace(/[\/ \.]/g, '_'); // Replace the slashes, spaces & dots with underscores

			/**
			 * Reduce filepath down to a managable name; get last 'src_' and take the remainder of the path with prefixes.
			 * If filepath contains codelib or angCodelib patterns use appropriate prefix otherwise
			 * file is from app so use build.config's prefix_app.
			 */
			var startIndex = propName.lastIndexOf('src_');
			startIndex = (startIndex === -1) ? 0 : startIndex + 4;
			var prefix = options.libPattern.test(file) ? options.prefix_lib : (options.anglibPattern.test(file) ? options.prefix_anglib : grunt.config('templates.prefix_app'));
			propName = prefix + propName.substring(startIndex);
			console.log("------>>       propName: " + propName);

			var data = grunt.file.read(file); // read contents of file
			/* jshint -W049 */ // Avoid "Unexpected escaped character '<' in regular expression" warnings for the following lines
			data = data.replace(/[ \s]*\</g, '<'); // remove leading whitespace before html tags
			data = data.replace(/\>[ \s]*/g, '>'); // remove trailing whitespace after html tags
			data = data.replace(/\"/g, '\\"'); // escape double quotes

			fileContents += 't["' + propName + '"] = "' + data + '";\n';
		});

		if (fileContents !== '') {
			grunt.file.write(grunt.config('build_dir') + '/' + grunt.config('templates.html') + '.js',
				'(function() {\n' +
				'"use strict";\n' +
				'var t = window.vm_h2jTemplates = window.vm_h2jTemplates || {};\n' +
				fileContents +
				'})();'
			);
		}
	});

	/** 
	 * The src input to this task is a list of html, js and css files. Each html file is processed for template strings.
	 * Where the template includes a scripts variable a list of script includes for each source js file will be included
	 * Where the template includes a styles variable a list of links for each source css file will be included
	 *
	 * The destination input is not used. The dir input is used instead for some reaso, as the place to copy the html files. - should use dest and then app.html can be mapped to app name in spec not hard coded
	 *
	 * The app.html file has Lo-Dash template strings. These are used to include the stylesheet and javascript sources
	 * script includes. The templates use dynamic names calculated in this Gruntfile.
	 * This task assembles the list into variables for the template to use and then uses copy to copy the file and as part
	 * of the copy process uses the grunt template task to process the templates.
	 */
	grunt.registerMultiTask('index', 'Process html template files', function() {

		function processHtmlFile( contents, path ) {	
			return grunt.template.process( contents, {
				data: {
					scripts: jsFiles,
					styles: cssFiles,
					version: grunt.config( 'pkg.version' ) + "_" + buildId,
					buildEnvironment: buildEnvironment
				}
			});
		}

		var buildEnvironment = this.target === 'build' ? "DEV" : "LIVE";
		//var tstamp = buildEnvironment=== 'DEV' ? '?' + new Date().getTime() : '';

		var dirRE = new RegExp('^(' + grunt.config('build_dir') + '|' + grunt.config('compile_dir') + ')\/', 'g');
		var jsFiles = filterForJS(this.filesSrc).map(function(file) {
			//file = file + tstamp;
			return file.replace(dirRE, '');
		});
		var cssFiles = filterForCSS(this.filesSrc).map(function(file) {
			//file = file + tstamp;
			return file.replace(dirRE, '');
		});

		this.files.forEach(function(file){
			//grunt.log.writeln('source files: ' + file.src);
			var htmlFiles = filterForHtml( file.src );

			grunt.log.writeln('dest:' + file.dest + '   html files: ' + htmlFiles);

			//if (htmlFiles && htmlFiles.length > 0) {
			for (var i=0; i < htmlFiles.length; i++) {
				//grunt.log.writeln(htmlFiles[i]);
				//var dest = this.data.dir + '/';
				//if (htmlFiles[i].indexOf('app.html') !== -1){
				//	dest += grunt.config('pkg.name') + '.html';
				//}
				var dest = file.dest;
				if (dest.indexOf('.') === -1){
					var slashPos = htmlFiles[i].lastIndexOf('/');
					if (slashPos === -1){
						dest += htmlFiles[i];
					} else {
						dest += htmlFiles[i].substring(slashPos);
					}
				}
				grunt.log.writeln(dest);
				grunt.file.copy(htmlFiles[i], dest , {
					process: processHtmlFile
				});
			}
			//}

			});

	});

	/**
	 * In order to avoid having to specify manually the files needed for karma to
	 * run, we use grunt to manage the list for us. The `karma/*` files are
	 * compiled as grunt templates for use by Karma. Yay!
	 */
	grunt.registerMultiTask('karmaconfig', 'Process karma config templates', function() {
		var jsFiles = filterForJS(this.filesSrc);

		grunt.file.copy('build/config/karma-unit.tpl.js', grunt.config('build_dir') + '/karma-unit.js', {
			process: function(contents, path) {
				return grunt.template.process(contents, {
					data: {
						scripts: jsFiles
					}
				});
			}
		});
	});

	/**
	 * Grunt task to adjust relative urls in scss files, due to
	 * 1) the sass files are merged and so their final released location is different from their location under src
	 * 2) the asset files the scss reference are moved from their location under src to under asset folder in the released app
	 *
	 * Taken from : grunt-css-urls (https://github.com/Ideame/grunt-css-urls)
	 * but adjusted to work for us.
	 *
	 * The task takes scss source files (we only have one main.scss) that have @import statements for other scss files. For each of these
	 * scss files it looks for url() values and adjusts these based on the location of scss file it is in and where it is pointing to.
	 *
	 * The different adjustments it reconises are:
	 * library file: based on the scss file location - the scss file and the asset file maintain their relative location and are moved to
	 *               be under the assets folder
	 * vendor files: as for library files
	 * other files:  these are other scss files and will be application scss files. Here each referenced url is inspected. If it is to an
	 *               item in the src/asset folder then the reerenced asset stays in the asset folder, but the sccs moves. Otherwise the asset
	 *               moves to the asset folder and the scss moves as well.  
	 */
	grunt.registerMultiTask("cssUrls", "Parses a given main css file with @import rules, iterates through them replacing url() relative references with a relative url to the main css file.", function() {

		var options = this.options({ // added destRoot option for use in baseDir - set default
			destRoot: 'tempcss'
		});

		this.files.forEach(function(file) {
			var contents = file.orig.src.filter(function(filepath) {
				if (!grunt.file.exists(filepath)) {
					grunt.log.warn('Source file "' + filepath + '" not found.');
					return false;
				} else {
					return true;
				}
			}).map(function(filepath) {
				var importContents = grunt.file.read(filepath);

				importContents.replace(/@import\s+'([^']+)/gim, function(match, location) {
					location = location.replace(/'|"/g, '');
					var filename = path.resolve(path.dirname(filepath), location);
					var content = grunt.file.read(filename).toString(); // sometimes css is interpreted as object
					grunt.log.writeln(' Parsing ' + location);

					var css = content.replace(/url(?:\s+)?\(([^\)]+)\)/igm, function(match, urlVal) {
						var newUrl;
						url = urlVal.replace(/'|"/g, '');
						var absUrl = path.resolve(path.dirname(filename), url);
						// set destination root based on type of import
						destRoot = options.libPattern.test(location) ? options.libRoot : (options.vendorPattern.test(location) ? options.vendorRoot : 
							(options.otherAssetUrlAssetPattern.test(urlVal) ? options.otherAssetRoot : options.otherOtherRoot));
						var absDestRoot = path.resolve(path.join(process.cwd(), destRoot)); // Match destination root folder
						grunt.log.writeln(' absDestRoot: ' + absDestRoot);

						if (/^data:/.test(url) || /^\\/.test(url)) {
							grunt.log.writeln(" - Data url, url ignored");
							newUrl = url;
						} else
						if (/^\//.test(url) || /^\\/.test(url)) {
							grunt.log.writeln(" - Absolute urls are not changed, url ignored => " + url);
							newUrl = url;
						} else
						if (/^(\s+)?$/.test(url)) {
							grunt.log.writeln(" - Empty url, url ignored => " + url);
							newUrl = url;
						} else
						if (/#/.test(url) && !/\?#iefix|svg#/.test(url)) {
							grunt.log.writeln(" - Anchors not allowed, url ignored => " + url);
							newUrl = url;
						} else
						if (absUrl.indexOf(absDestRoot) !== 0) {
							grunt.log.writeln('abs url: ' + path.resolve(path.dirname(filename), url));
							grunt.log.writeln(" - Must be under destination root (" + destRoot + "), url ignored => " + url);
							newUrl = url;
						} else {
							newUrl = absUrl.replace(absDestRoot, '.');
							newUrl = newUrl.replace(/\\/g, "/"); // make windows paths with \ in url paths with / in
							grunt.log.writeln(' Resolved: ' + url + '  to: ' + newUrl);
						}

						return util.format("url('%s')", newUrl); // Added quotes around the path to work with grunt-sass
					});

					grunt.file.write(filename, css);
				});
			});
		});
	});

};
