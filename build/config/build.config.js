/**
 * This file/module contains all configuration for the build process.
 */

(function(module) {
	"use strict";
	
	var appFilesJsLib = [
			// load library files first and in required order

			// HTML Library
//                        'src/lib/sm-dtv-html-codelib/src/utils/arrayUtils.js',
                        'src/lib/sm-dtv-html-codelib/src/utils/util.js',
			'src/lib/sm-dtv-html-codelib/src/utils/remedial.js',
			'src/lib/sm-dtv-html-codelib/src/utils/string.js',
			'src/lib/sm-dtv-html-codelib/src/utils/dateTime.js',
			'src/lib/sm-dtv-html-codelib/src/lib/init.js',
			'src/lib/sm-dtv-html-codelib/src/logger/log.js',
			'src/lib/sm-dtv-html-codelib/src/error/VmError.js',
			'src/lib/sm-dtv-html-codelib/src/error/errorUtil.js',
			'src/lib/sm-dtv-html-codelib/src/net/net.js',
			'src/lib/sm-dtv-html-codelib/src/net/url.js',
			'src/lib/sm-dtv-html-codelib/src/utils/system.js',
			'src/lib/sm-dtv-html-codelib/src/utils/sound.js',
			'src/lib/sm-dtv-html-codelib/src/utils/domUtil.js',
			'src/lib/sm-dtv-html-codelib/src/utils/util.js',
			'src/lib/sm-dtv-html-codelib/src/keys/keys.js',
//			'src/lib/sm-dtv-html-codelib/src/utils/listUtil.js',
			'src/lib/sm-dtv-html-codelib/src/tivo/tivo.js',
			'src/lib/sm-dtv-html-codelib/src/dax/dax.js',
			'src/lib/sm-dtv-html-codelib/src/utils/fileLoader.js',
			'src/lib/sm-dtv-html-codelib/src/utils/jsonLoader.js',
			'src/lib/sm-dtv-html-codelib/src/config/configLoader.js',
			// 'src/lib/sm-dtv-html-codelib/src/player/*.js',
			'src/lib/sm-dtv-html-codelib/src/appHistory/appHistory.js',
			'src/lib/sm-dtv-html-codelib/src/multitap/multitap.js',
//			'src/lib/sm-dtv-html-codelib/src/uiComponents/about/about.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/scrollingField/scrollingField.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/list/list.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/popup/popup.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/basicPopup/basicPopup.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/optionPopup/optionPopup.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/loadingScreen/loadingScreen.js',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/pinPrompt/pinPrompt.js'
//                        'src/lib/sm-dtv-html-codelib/src/uiComponents/carousel/carousel.js'

			// Angular Library
			//'src/lib/sm-dtv-html-angCodelib/**/*.js'
		];
	
	module.exports = {
	  /**
	   * The `build_dir` folder is where our projects are compiled during
	   * development and the `compile_dir` folder is where our app resides once it's
	   * completely built.
	   */
	  build_dir: 'dev',
	  compile_dir: 'bin',

	  /**
	   * Directory for temporary use to process CSS files.
	   * Will update relative URLs to work in builds.
	   */
	  tempcss_dir: 'tempcss',

	  /**
	   * Filenames for the template files, without the '.js'
	   */
	  templates: {

		html: 'template',
		ang_app: 'templates-ang-app',
		ang_lib: 'templates-ang-lib',

		/**
		 *  prefix_app needs to be an empty string for a standard app build, but
		 *  needs to be 'lib_' if doing a lib component build to match the library prefix
		 */
		prefix_app: ''
	  },

	  /** This is a collection of file patterns that refer to our app code (the
	   * stuff in `src/`). These file paths are used in the configuration of
	   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
	   * our reusable components' (`src/lib`) template HTML files, while
	   * `atpl` contains the same, but for our app's code. `html` is just our
	   * main HTML file, `sass` is our main stylesheet, and `unit` contains our
	   * app's unit tests.
	   */
	  app_files: {
		js: appFilesJsLib.concat( 
			'src/app/**/*.js',
			'!src/**/*.spec.js'
		),

		// all app_files used in the unit test build, test globals, lib js, app js, spec js  
		jsunit: [
			'src/jsunitGlobals.js',
			appFilesJsLib,
			'src/app/**/*.js',
		],

		specjs: [ 'src/**/*.spec.js' ],

		atpl: [ 'src/app/**/*.tpl.html' ],
		ctpl: [ 'src/lib/**/*.tpl.html' ],

		// add library template files individually so only the ones you need are built into your app. 
		tmpl: [ 
			//'src/app/*.tmpl.html'
			'src/lib/sm-dtv-html-codelib/src/uiComponents/loadingScreen/*.tmpl.html',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/list/list.tmpl.html',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/carousel/*.tmpl.html',
			'src/lib/sm-dtv-html-codelib/src/uiComponents/pinPrompt/*.tmpl.html',		
			'src/app/passwordPrompt/*.tmpl.html'		
		 ],
		html: ['src/app/*.html', '!src/app/app.html'],
		mainHtml: 'src/app/app.html', // the single main html file that will be renamed to appName.html
		scss: 'src/scss/main.scss',
		assets: ['src/app/**/*.png'] // list any app assets not in the app/assets/ directory

	  },

	  /**
	   * This is the same as `app_files`, except it contains patterns that
	   * reference code library files that we need to place into the build.
	   * List either individual files or use wildcards to include more, 
	   * eg <folder_path>/*.png for all png files in a folder.
	   */
	  library_files: {
		//js: [],				// not used as yet -- JS files are included in app_files.js (see above)
		assets: [
			//'src/lib/sm-dtv-html-codelib/src/fonts/HelveticaNeueLT-Light.ttf',
			//'src/lib/sm-dtv-html-codelib/src/fonts/HelveticaNeueLT-Thin.ttf'
		]
	  },

	  /**
	   * Setting to include/exclude the app version number in the built file names
	   */
	  addVersionToFilenames: true,


	  /**
	   * This is a collection of files used during testing only.
	   */
	  test_files: {
		js: [
		  //'vendor/bower/angular-mocks/angular-mocks.js'
		]
	  },

	  /**
	   * This is the same as `app_files`, except it contains patterns that
	   * reference vendor code (`vendor/`) that we need to place into the build
	   * process somewhere. While the `app_files` property ensures all
	   * standardized files are collected for compilation, it is the user's job
	   * to ensure non-standardized (i.e. vendor-related) files are handled
	   * appropriately in `vendor_files.js`.
	   *
	   * The `vendor_files.js` property holds files to be automatically
	   * concatenated and minified with our project source files.
	   *
	   * The `vendor_files.css` property holds any CSS files to be automatically
	   * included in our app.
	   *
	   * The `vendor_files.assets` property holds any assets to be copied along
	   * with our app's assets. This structure is flattened, so it is not
	   * recommended that you use wildcards.
	   */
	  vendor_files: {
		js: [
		  //'vendor/other/dash/dash.all.js',
                  'vendor/bower/jquery/dist/jquery.js'
		  //'vendor/jquery/jquery.js',
		  //'vendor/bower/angular/angular.js',
		  //'vendor/bower/angular-bootstrap/ui-bootstrap-tpls.min.js',
		  //'vendor/bower/placeholders/angular-placeholders-0.0.1-SNAPSHOT.min.js',
		  //'vendor/bower/angular-ui-router/release/angular-ui-router.js',
		  //'vendor/bower/angular-ui-utils/modules/route/route.js'
		],
		css: [
		],
		assets: [
		]
	  },

	  /**
	   * Adding the module.prefix and module.suffix settings here from the 
	   * BoilerPlate's separate files.
	   */
	  module: {
		prefix: '(function ( window, angular, undefined ) {',
		suffix: '})( window, window.angular );'
	  },

	  /**
	   * Publish Config files will be released to publishConfig/htmlApps/<appName>/<versionName>/
	   * where the appName comes from the app, and the versionName is set here.
	   * Expected to be in the format YYYYMMDD.
	   */
	  publishConfig: {
		versionName: '20150101'
	  },
	};

})(module);