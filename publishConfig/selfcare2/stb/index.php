<?php

$stbmac = $_GET['stbmac'];
$action = $_GET['action'];
$acct = $_GET['acct'];
$keepOrder = $_GET['keepOrder'];
$products = $_GET['products'];
$positions = $_GET['positions'];
$chars = $_GET['chars'];

if ( !isset( $action ) )
{
	returnAnError( 'SCS001');
	die();
}


if (!isset( $stbmac ) )
{
	returnAnError('SCS000');
	die();
}


switch ( $action )
{
	case 'get_options':
		returnSampleOptions();
	break;

	case 'get_account':
//            returnAnError('SCS002');
		getAccount();
	break;

	case 'check_password':
                checkPassword($positions, $chars);
	break;
	
        case 'process_hit':
		processHit();
	break;
    
	case 'create_work_order':
		if ( !isset($acct) ) 
		{
			returnAnError('SCS041');die();
		} 

		if ( !isset($keepOrder) )
		{
			returnAnError('SCS042');die();
		}

		if (!isset($products))
		{
			returnAnError('SCS043');die();
		}

		returnAValidWorkOrder();
	break;

	default :
		returnAnError('SCS002');
	break;
}

function returnSampleOptions()
{
	echo 'acct = "24,123546789";errorCode = null;e[e.length] = "A102313,Box Nation,Remove,0";e[e.length] = "A100070,Asian Mela,Remove,0";e[e.length] = "A100896,Picture Box,Add,0";e[e.length] = "A100078,Baby TV,Add,0";	e[e.length] = "A100048,MUTV,Add,0";e[e.length] = "A101928,Racing UK,Add,0";e[e.length] = "A100845,Sky Movies Collection,Add,0";';
}

function returnAValidWorkOrder()
{
	echo 'errorCode = null;
	monthly = "80.00";
	totalMonthly = "80.00";
	prorated = "10.00";';
}

function returnAnError( $code )
{
	echo 'errorCode="' . $code .'"';
}

function processHit(){
    echo "errorCode = null;";
}

function getAccount(){
    $password = "TKD88";
    $firstRandNum = rand(1, strlen($password) - 2);
    $secondRandNum = rand($firstRandNum + 1, strlen($password) - 1);
    $thirdRandNum = rand($secondRandNum + 1, strlen($password));
    
    echo 'acct = "24,123546789";pwIdxs=new Array('.$firstRandNum.','.$secondRandNum.','.$thirdRandNum.');errorCode = null;';
}

function checkPassword($positions, $chars){
    if ( !isset($positions) ) 
    { 
        returnAnError('"SCS044"');die();
    }
    else if(!isset($chars))
    {
        returnAnError('"SCS045"');die();
    }    
    else
    {    
        verifyPassword($positions, $chars);
    }
}

function verifyPassword($positions, $chars){
    $password = "TKD88";
    $verified = true;
    $positionsArray = explode(",", $positions);
    $charsArray = explode(",", $chars);

    for($i = 0; $i < count($positionsArray); $i++){
        $verified = ($password[($positionsArray[$i] - 1)]) === ($charsArray[$i]);

        if(!$verified) {
            break;
        }
    } 

    $errorCode = ($verified? 'null':'"SCS046"');
    $retries = $verified? 0:2;

    echo 'pwVerified = '.($verified? 'true':'false').';retries = '.$retries.';errorCode = '.$errorCode.';';
}

?>