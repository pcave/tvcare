:: rgrunt - the Grunt batch file
:: 
:: Calls grunt with the param passed in as its task.
:: This allows us to have the Gruntfile.js in the build/ dir.
:: This file must be in the project root.
:: 
:: Parameter:
:: 1) Task name

@echo off

echo == rgrunt == running grunt with task: %*

grunt --base . --gruntfile build\Gruntfile.js %*
