#!/bin/bash

# rgrunt - the Grunt shell script
# 
# Calls grunt with the params passed into Gruntfile, the first being the task.
# This allows us to have the Gruntfile.js in the build/ dir.
# This file must be in the project root.
# 
# Parameter:
# 1) Task name
# 2) Other params (optional)

echo "rgrunt - running grunt with task: $1"
grunt --base . --gruntfile build/Gruntfile.js "$@"
